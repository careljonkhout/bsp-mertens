cores=16
base=1
exp=9
fname=out_c${cores}_b${base}_e${exp}_`date +"%Y-%m-%d_%H:%M:%S"`
echo $fname
(time ./bsp_mertens $cores $base $exp) &> $fname 
