#include <inttypes.h>

#ifndef aux__
#define aux__

#define false 0
#define true 1

#define mu_type int_least8_t
#define primes_type int64_t
#define result_type int64_t
#define bound_type uint64_t

#define PROTECT_ARRAYS false
#define verbose true
#define seq_m_verbose false
#define primes_verbose false
#define progress_indicator false

#define i128_string_length 41

const int L;

bound_type power_of_ten(int base, int exp);
int64_t max(int64_t a, int64_t b);
int64_t min(int64_t a, int64_t b);
int64_t a(int64_t k,int64_t L);

int64_t read_array(int64_t* array, int64_t index,
	int64_t max, int line);
mu_type read_mu(mu_type* array, int64_t index,
	int64_t max, int line);
void write_array(int64_t* array, int64_t index,
	int64_t val, int64_t max, int line);
void write_mu(mu_type* array, int64_t index,
	mu_type val, int64_t max, int line);



// prints a bound_type, which could be a __int128, into buf
// a buf of 41 bytes should be long enough
char* sprint_result(result_type i, char* buf);
char* sprint_bound(bound_type i, char* buf);

void* array_alloc(int_fast32_t n,int elementsize,char* error_message);
char* sprint128(bound_type i128,char* buf);
int64_t l(int64_t y, int64_t k);
void print_largest_primes(char* prime, int32_t primebound);

int  compute_primes_and_logprimes( int primebound,
		primes_type* primesbound, mu_type* logprimes,
		int pid);
void compute_mu(int64_t a, int64_t b,
		primes_type* primes, mu_type* logprimes,
		int primecount, mu_type* mu, int progress);
void compute_mertens_block(int64_t a, int64_t b,
		 primes_type* primes, mu_type* logprimes,
		 int primecount, mu_type* mu, int64_t* mertens, 
		 int64_t previous_mertens);

#endif
