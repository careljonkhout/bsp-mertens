#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <inttypes.h>
#include "mcbsp.h"
#include "mcbsp-affinity.h"

#define max_i 255
#define min_x -2.0
#define max_x 0.9
#define min_y -1.25
#define max_y 1.25
#define xsteps 161
#define ysteps 51

int ncores;

int mandel_point(double x0, double y0) {
	double x=x0;
	double y=y0;
	int i=0;
	while (x*x + y*y < 4 && i < max_i) {
		double xx = x*x-y*y + x0;
		double yy = 2*x*y + y0;
		if (xx==x&&yy==y) {
			i = max_i;
			break;
		}
		x=xx;y=yy;
		i++;
	}
	return i;
}


void bsp_mandel() {
	bsp_begin(ncores);
	int s = bsp_pid();
	double xstep = (max_x - min_x) / (xsteps -1);
	double ystep = (max_y - min_y) / (ysteps -1);
	double y = max_y;
	for (int i=0;i<ysteps;i++) {
		double x = min_x;
		for (int j=0;j<xsteps;j++) {
			//printf("i:%d j:%d x:%.2f y:%.2f \n",i,j,x,y);
			int ii = mandel_point(x,y);
			printf(ii > 128 ? "X" : " ");
			//if (ii > 128) {printf("%.3d ",ii);} else {printf("    ");}
			x += xstep;
		}
		printf("\n");
		y -= ystep;
	}
	bsp_end();
} // end bsp_mertens


int main(int argc, char* argv[]) {
    bsp_init(bsp_mandel, argc, argv);
	mcbsp_set_affinity_mode(COMPACT);

	ncores = 1;
	bsp_mandel();
    return 0;
} // end main
