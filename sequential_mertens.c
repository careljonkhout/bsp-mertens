#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <inttypes.h>
#include "mcbsp.h"
#include "mcbsp-affinity.h"
#include "aux.h"


result_type sequential_mertens(bound_type bound) {
	int64_t u = floor(pow(bound,1/3.0)*
		pow(log(log(bound)),2/3.0));
	if (seq_m_verbose) printf("seq_m:u:%" PRId64 " ", u);

	int64_t max_m = bound/u+1;
	if (seq_m_verbose) printf("seq_m:max_m:%" PRId64 "\n", max_m);

	int primebound = ceil(sqrt(max_m))+1;
	primebound = primebound > 10000 ? primebound : 10000;
	if (seq_m_verbose) printf("primebound:%d ", primebound);

	// 2 n / log(n) is a rough upper bound estimate of 
	// number of number of primes below n
	int primesbound = ceil(2*primebound/log(primebound)); 
	if (seq_m_verbose) printf("primesbound:%d ", primesbound);
	int64_t L = u;    
	if (seq_m_verbose) printf("L:%" PRId64 " ", L);	

	char* prime     = array_alloc(primebound,  sizeof(char),
		"Could not allocate prime.");
	primes_type* primes = array_alloc(primesbound, sizeof(primes_type),
		"Could not allocate primes.");
	mu_type* logprimes = array_alloc(primesbound, sizeof(mu_type),
		"Could not allocate logprimes.");
	
	// values WILL fit in int32, since we expect to go 
	// in the best case senario to say 10**24
	// hence smallmertens will go up to 10**16
	// and up to 10**20 the mertens function IS bounded by sqrt(n)
	// hence values are bounded by 10**8
	// futhermore, we are ok for computing M(n) for n up to 10**27

	for (int i=0;i<primebound;i++) {
		prime[i] = true;
	}
	// sieve of eratosthenes
	prime[0]=prime[1]=false;
	int sieve_until = sqrt(primebound);
	if (seq_m_verbose) printf("sieve_until:%d\n", sieve_until);
	// <= is really neccesary
	for (int i=2;i<=sieve_until;i++) {
		if (prime[i]) {
			for (int j=i*i;j<primebound;j+=i) {
				prime[j] = false;
			}
		}
	}
	
	// print_largest_primes(prime,primebound);

	// populate primes and logprimes arrays
	int primecount=0;
	for (int i=2;i<primebound;i++) {
		if (prime[i]) {
			primes[primecount] = i;
			logprimes[primecount] = ((mu_type) (floor(log2(i)))) |(mu_type)1;
			primecount++;
		}
	}
	if (primecount <= 0) {
		// overflow occurred, very unlikely
		// ( expected to occur when bound >= 10^27
		// if primecount is 32 bits )
		// but better safe than sorry
		printf("primecount overflowed \n");
		bsp_abort("");
		exit(1);
	}
	if (seq_m_verbose) printf("primecount: %d\n",primecount);
	// compute mu and mertens
	int64_t max_mu = u+1;
	if (seq_m_verbose) printf("max_mu %" PRId64 "\n",max_mu);
	mu_type* mu     = array_alloc(max_mu,sizeof(mu_type),
		"Could not allocate mu.");

	compute_mu(0,max_mu,primes,logprimes,primecount,mu,false);
	if (seq_m_verbose) printf("computed mu\n");
	int64_t* mertens = array_alloc(L,sizeof(int64_t),
		"Could not allocate mertens");
	mu_type* mu_for_mertens = array_alloc(L,sizeof(mu_type),
		"Could not allocate mu_for_mertens");
	// compute S_1 (as in Rivat)
	result_type S1 = 0;
	result_type S2 = 0;
	int64_t last_mertens = 0;
	int64_t tt = floor(sqrt(bound))+1;
	int64_t mertens_u;
	int percent = 0;
	int part_per_line = 0;
	for (int64_t kk=0;kk<=bound/u/L;kk++) {
		// progress indicator 1
		if (progress_indicator) {
			if (kk < 10) {
				printf("kk:%" PRId64 " ", kk);
				fflush(stdout);
			}
			if (kk==10) {
				printf("\n");	
			}
			// progress indicator 2
			// does not work very well
			// since apparently the iteration for kk==0
			// takes about as long as all the other iterations
			// combined
			if ((100*kk*u*L)/bound > percent) {
				percent=(100*kk*u*L)/bound;
				printf("%d%% ",percent); fflush(stdout);
				if (percent/20 > part_per_line) {
					printf("\n");
					part_per_line++;
				}
			}
		}
		// compute the block of mertens values
		// that are needed for this iteration
		compute_mertens_block(a(kk,L),a(kk+1,L),primes,logprimes,
			primecount,mu_for_mertens,mertens,last_mertens);
		// store the last value of the array
		// so we can compute the next block
		// in the next iteration
		last_mertens = mertens[L-1];
		if (a(kk,L) <= u && u < a(kk+1,L)) {
			mertens_u = read_array(mertens,u-a(kk,L),L,346);
		}
		int64_t m0 =   max(1,bound/a(kk+1,L)/a(kk+1,L));
		for (int64_t m=m0;m<=u;m++) {
			if (mu[m]) {
				result_type sum = 0;
				int64_t n0 =   max(u/m+1,         bound/m/a(kk+1,L)+1);
				int64_t nmax = min(sqrt(bound/m), bound/m/a(kk  ,L)  );
				for (int64_t n=n0;n<=nmax;n++) {
					sum += read_array(mertens,bound/(n*m)-a(kk,L),L,355);
				}
				S1 += ((int64_t)mu[m]) * sum;
			}			
		}
		int64_t k0   = a(kk,L);
		int64_t kmax = min(a(kk+1,L),tt);
		for (int64_t k=k0;k<kmax;k++) {
			if (progress_indicator && k < 10) {
				printf("k:%" PRId64 " ",k); fflush(stdout);
			}
			result_type sum = 0;
			int64_t bound1 = bound/(k*k); 
			bound1 = bound1 < u ? bound1 : u;
			for (int m=1;m<=bound1;m++) {
				if (read_mu(mu,m,u+1,370)) {
					sum += ((int64_t)read_mu(mu,m,u+1,371)) * l(bound/m,k);
				}
			}
			S2 += read_array(mertens,k-a(kk,L),L,374) * sum;
		}
	}
	
	free(prime);
	free(primes);
	free(logprimes);
	free(mu);
	free(mertens);
	free(mu_for_mertens);
	if (progress_indicator) printf("\n");
	return mertens_u-S1-S2;
} // end sequential_mertens


