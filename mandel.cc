#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <inttypes.h>
#include "mcbsp.h"
#include "mcbsp-affinity.h"

class Mandel {

	
	 max_i 255
	#define min_x -2
	#define max_x 2
	#define min_y -2
	#define max_y 2
	#define xsteps 11
	#define ysteps 21

	int ncores;

	void bsp_mandel() {
		bsp_begin(ncores);
		int s = bsp_pid();
		double xstep = (max_x - min_x) / (xsteps -1);

		for 
		bsp_end();
	} // end bsp_mertens

	int mandel_point(double x0, double y0) {
		double x=x0;
		double y=y0;
		int i=0;
		while (x*x + y*y < 4 && i < max_i) {
			double xx = x*x-y*y + x0;
			double yy = 2*x*y + y0;
			if (xx==x&&yy==y) {
				i = max_i;
				break;
			}
			x=xx;y=yy;
			i++;
		}
		return i;
	}

};

int main(int argc, char* argv[]) {
    bsp_init(bsp_mandel, argc, argv);
	mcbsp_set_affinity_mode(COMPACT);

	ncores = 1;
	bsp_mandel();
    return 0;
} // end main
