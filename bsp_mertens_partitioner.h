#include "aux.h"

#ifndef bsp_mertens_partitioner_
#define bsp_mertens_partitioner_

/*
 * finds a partition of the data,
 * writes the computed partition into the array partition
 */
 
int64_t find_partition(bound_type x, int64_t u, int n_cores, int64_t* partition);

/*
 * prints a partition found by find_partition on the standard output
 */
int64_t print_partition(bound_type x, int64_t u, int n_cores, int64_t* partition);
 
#endif  // ifndef bsp_mertens_partitioner_