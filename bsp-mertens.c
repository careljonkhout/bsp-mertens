#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <inttypes.h>
#include "mcbsp.h"
#include "mcbsp-affinity.h"
#include "bsp_mertens_partitioner.h"
#include "sequential_mertens.h"
#include "aux.h"

bound_type bound;
int ncores;


void bsp_mertens() {
	bsp_begin(ncores);
	int s = bsp_pid();

	int64_t u = floor(pow(bound,1/3.0)*
		pow(log(log(bound)),2/3.0));
	if (s==0 && verbose) printf("u:%" PRId64 " ", u);

	int64_t max_m = bound/u+1;
	if (s==0 && verbose) printf("max_m:%" PRId64 "\n", max_m);

	int primebound = ceil(sqrt(max_m))+1;
	primebound = primebound > 10000 ? primebound : 10000;
	if (s==0 && verbose) printf("primebound:%d ", primebound);

	// 2 n / log(n) is a rough upper bound estimate of 
	// number of number of primes below n
	int primesbound = ceil(2*primebound/log(primebound)); 
	if (s==0 && verbose) printf("primesbound:%d ", primesbound);
	if (s==0 && verbose) printf("L:%" PRId64 " ", L);	

	char* prime     = array_alloc(primebound,  sizeof(char),
		"Could not allocate prime.");
	primes_type* primes = array_alloc(primesbound, sizeof(primes_type),
		"Could not allocate primes.");
	mu_type* logprimes = array_alloc(primesbound, sizeof(mu_type),
		"Could not allocate logprimes.");

	for (int i=0;i<primebound;i++) {
		prime[i] = true;
	}
	// sieve of eratosthenes
	prime[0]=prime[1]=false;
	int sieve_until = sqrt(primebound);
	if (s==0 && verbose) printf("sieve_until:%d\n", sieve_until);
	// <= is really neccesary
	for (int i=2;i<=sieve_until;i++) {
		if (prime[i]) {
			for (int j=i*i;j<primebound;j+=i) {
				prime[j] = false;
			}
		}
	}
	
	// print_largest_primes(prime,primebound);

	// populate primes and logprimes arrays
	int primecount=0;
	for (int i=2;i<primebound;i++) {
		if (prime[i]) {
			primes[primecount] = i;
			logprimes[primecount] = ((mu_type) (floor(log2(i)))) |(mu_type)1;
			primecount++;
		}
	}
	if (primecount <= 0) {
		// overflow occurred, very unlikely
		// ( expected to occur when bound >= 10^27 will
		// if primecount is 32 bits )
		// but better safe than sorry
		printf("primecount overflowed \n");
		bsp_abort("");
		exit(0);
	}
	if (s==0 && verbose) printf("primecount: %d\n",primecount);
	// compute mu and mertens
	int64_t max_mu = u+1;
	if (s==0 && verbose) printf("max_mu %" PRId64 "\n",max_mu);
	mu_type* mu     = array_alloc(max_mu,       sizeof(mu_type),
		"Could not allocate mu.");

	compute_mu(0,max_mu,primes,logprimes,primecount,mu,false);
	if (s==0 && verbose) printf("computed mu\n");
	int64_t* mertens = array_alloc(L,sizeof(int64_t),
		"Could not allocate mertens");
	mu_type* mu_for_mertens = array_alloc(L,sizeof(mu_type),
		"Could not allocate mu_for_mertens");
	// compute S_1 (as in Rivat)
	result_type S1 = 0;
	result_type S2 = 0;
	result_type last_mertens = 0;
	int64_t tt = floor(sqrt(bound))+1;
	int64_t mertens_u;
	if (s==0) {
		mertens_u = sequential_mertens(u);
	}

	// for progress indicator
	int percent = 0;
	int part_per_line = 0;

	int64_t partition[ncores-1];
	for (int i=1;i<ncores;i++) {
		partition[i-1] = bound/u/ncores*i;
	}
	find_partition(bound,u,ncores,partition);


	int64_t a0 = bound/u/ncores*s;
	int64_t a_max = bound/u/ncores*(s+1);

	if (s==0) {
		a0 = 0;
	} else {
		a0 = partition[s-1];
	}


	if (s==ncores-1) {
		a_max = bound/u+1;
	} else {
		a_max = partition[s];
	}
	
	if (s == 0) {
		last_mertens = 0; 
	} else {
		last_mertens = sequential_mertens(a0);
	}
	for (int i=0;i<ncores;i++) {
		if (s==i) {
			printf("pid:%d block:%ld %ld\n",i,a0,a_max);
		}
		bsp_sync();
	}

	int64_t kk_max = ceil( ((double) (a_max-a0)) / L );

	for (int64_t kk=0;kk<kk_max;kk++) {


		// compute the block of mertens values
		// that are needed for this iteration
		int64_t low  = a0+a(kk,L);
		int64_t high = min(a0+a(kk+1,L), a_max+1);
		compute_mertens_block(low,high,primes,logprimes,
			primecount,mu_for_mertens,mertens,last_mertens);
		// store the last value of the array
		// so we can compute the next block
		// in the next iteration
		last_mertens = mertens[L-1];

		//if (u * a(kk,L) <= bound) {
			int64_t counter = 0;
			int64_t m0 =   max(1,bound/high/high);
			for (int64_t m=m0;m<=u;m++) {
				if (mu[m]) {
					result_type sum = 0;
					int64_t n0 =   max(u/m+1,         bound/m/high+1 );
					int64_t nmax = min(sqrt(bound/m), bound/m/low    );
					for (int64_t n=n0;n<=nmax;n++) {
						sum += read_array(mertens,bound/(n*m)-low,L,355);
					}
					S1 += ((int64_t)mu[m]) * sum;
				}			
			}
		//}
		int64_t k0   = low;
		int64_t kmax = min(high,tt);
		for (int64_t k=k0;k<kmax;k++) {
			if (progress_indicator && k < 10 && s==0) {
				printf("k:%" PRId64 " ",k); fflush(stdout);
			}
			result_type sum = 0;
			int64_t bound1 = bound/(k*k); 
			bound1 = bound1 < u ? bound1 : u;
			for (int m=1;m<=bound1;m++) {
				if (read_mu(mu,m,u+1,370)) {
					sum += ((int64_t)read_mu(mu,m,u+1,371)) * l(bound/m,k);
				}
			}
			S2 += read_array(mertens,k-low,L,374) * sum;
		}
	}
	//printf("                   M(u) S1      S2        result\n");
	//printf("By casting to int %d %d %d %d \n",(int)mertens_u,(int)S1,(int)S2,
		//(int)(mertens_u-S1-S2));

	
	free(prime);
	free(mu);
	free(mertens);

	result_type S12 = S1+S2;
	result_type S12s[ncores];
	bsp_push_reg(S12s,ncores*sizeof(result_type));
	bsp_sync();
	bsp_put(0,&S12,S12s,s*sizeof(result_type),sizeof(result_type));
	bsp_sync();
	bsp_pop_reg(S12s);
	
	if (s==0) {
		char buf[41];
		result_type total_S12 = 0;
		for (int i=0;i<ncores;i++) {
			total_S12 += S12s[i];
		}
		if (progress_indicator) printf("\n");
		printf("final result: %s\n", sprint_result(mertens_u-total_S12,buf));
	}
	
	bsp_end();
} // end bsp_mertens

int main(int argc, char* argv[]) {
    bsp_init(bsp_mertens, argc, argv);
	mcbsp_set_affinity_mode(COMPACT);
	bound = power_of_ten(1,12);
	ncores = 4;
	int input_ok = true;
	if (argc >= 4) {
		int base;
		int exp;
		input_ok &= (sscanf(argv[1],"%d",&ncores) != EOF);
		input_ok &= (sscanf(argv[2],"%d",&base) != EOF);
		input_ok &= (sscanf(argv[3],"%d",&exp ) != EOF);
		bound = power_of_ten(base,exp);
	}
	else if (argc >= 3) {
		int base;
		int exp;
		input_ok &= (sscanf(argv[1],"%d",&base) != EOF);
		input_ok &= (sscanf(argv[2],"%d",&exp ) != EOF);
		bound = power_of_ten(base,exp);
	}
	else if (argc >= 2) { // note: argv[0] = executable file name
		input_ok &= (sscanf(argv[1],"%" SCNd64,&bound) != EOF);
	} 
	if (! input_ok) {
		printf("input error");
		return 1;
	} else if (
		bound > 10ull*1000ll*1000ll*1000ll*1000ll*1000ll*1000ll+1) {
		printf("bound too big");
		return 1;
	}

	int exp = floor(log10(bound));
	int base = floor(bound/pow(10,exp));
	printf("cores:%d ",ncores);
	char buf[i128_string_length];
	sprint_bound(bound,buf);
	printf("bound:%s=%d*10^%d\n",buf,base,exp);
	bsp_mertens();
    return 0;
} // end main