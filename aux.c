#include <inttypes.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "mcbsp.h"
#include "aux.h"


const int L = 4*1024*1024/10/2;

// computes base*10^exp
bound_type power_of_ten(int base, int exp) {
	bound_type result = base;
	for (int i=0;i<exp;i++) {
		result *= 10;
	}
	return result;
}


int64_t max(int64_t a, int64_t b) {return a>b?a:b;}
int64_t min(int64_t a, int64_t b) {return a<b?a:b;}


int64_t a(int64_t k,int64_t L) {
	return k*L+1;
	// we must add one, or else division by zero will occur
} // end a

// prints a bound_type, which could be a __int128, into buf
// a buf of 41 bytes should be long enough
char* sprint_result(result_type i, char* buf) {
	int sign = i < 0 ? -1 : 1;
	i *= sign;
	result_type original_i = i;
	int size = sign == -1 ? 1 : 0;
	while (i > 0) {
		size++;
		i/=10;
	}
	buf[size] = '\0';
	int index = size-1;
	while(original_i > 0) {
		buf[index] = '0' + original_i%10;
		index--;
		original_i/=10;
	}
	if (sign == -1) {
		buf[index] = '-';
	}
	return buf;
} // end sprint_result

char* sprint_bound(bound_type i, char* buf) {
	int sign = i < 0 ? -1 : 1;
	i *= sign;
	bound_type original_i = i;
	int size = sign == -1 ? 1 : 0;
	while (i > 0) {
		size++;
		i/=10;
	}
	buf[size] = '\0';
	int index = size-1;
	while(original_i > 0) {
		buf[index] = '0' + original_i%10;
		index--;
		original_i/=10;
	}
	if (sign == -1) {
		buf[index] = '-';
	}
	return buf;
} // end sprint_bound

/* reads an element of an int64_t array, with the option of 
   bounds checking, which is enabled by PROTECT_ARRAYS */
int64_t read_array(int64_t* array, int64_t index,
	int64_t max, int line) {
	if (!PROTECT_ARRAYS || 0 <= index && index < max) {
		return array[index];
	} else {
		printf("read_array: Array index out of bounds "
			"at line %d, index: %" PRId64 " max: %" PRId64 "\n", line, index, max);

		bsp_abort("");
		exit(0);
	}
}

/* reads an element of an mu_type array, with the option of 
   bounds checking, which is enabled by PROTECT_ARRAYS */
mu_type read_mu(mu_type* array, int64_t index,
	int64_t max, int line) {
	if (!PROTECT_ARRAYS || 0 <= index && index < max) {
		return array[index];
	} else {
		printf("read_mu: Array index out of bounds"
			"at line %d, index: %" PRId64 " max: %" PRId64 "\n", line, index, max);
		bsp_abort("");
		exit(0);
	}
}

/* writes an element to an int64_t array, with the option of 
   bounds checking, which is enabled by PROTECT_ARRAYS */
void write_array(int64_t* array, int64_t index,
	int64_t val, int64_t max, int line) {
	if (!PROTECT_ARRAYS || 0 <= index && index < max) {
		array[index] = val;
	} else {
		printf("write_array: Array index out of bounds"
			"at line %d, index: %" PRId64 " max: %" PRId64 "\n", line, index, max);

		bsp_abort("");
		exit(0);
	}
}

/* writes an element to a mu_type array, with the option of 
   bounds checking, which is enabled by PROTECT_ARRAYS */
void write_mu(mu_type* array, int64_t index,
	mu_type val, int64_t max, int line) {
	if (!PROTECT_ARRAYS || 0 <= index && index < max) {
		array[index] = val;
	} else {
		printf("write_mu: Array index out of bounds"
			"at line %d, index: %" PRId64 " max: %" PRId64 "\n", line, index, max);

		bsp_abort("");
		exit(0);
	}
}

/* allocates an array, printing error_message if malloc returns null */
void* array_alloc(int_fast32_t n,int elementsize, char* error_message){
    /* This function allocates a array of length n of 
       elements of length elementsize.
	   Based on a comparable method in bspedupack */
    void *p;
    if (n==0) {
        p = NULL;
    } else {
        p = malloc(n*elementsize);
        if (p==NULL) bsp_abort(error_message);
    }
    return p;
} // end array_alloc


/* computes prime numbers up to primebound
   storing primes is primes
   and storing  (floor(log2(p)) | 1) of every prime p in logprimes 
   (logprimes is used to used in the fast mobius sieve of Kuznetsov) */
int compute_primes_and_logprimes(int primebound,
	primes_type* primes,mu_type *logprimes,int s) {
	
	// values WILL fit in int32, since we expect to go 
	// in the best case senario to say 10**24
	// hence smallmertens will go up to 10**16
	// and up to 10**20 the mertens function IS bounded by sqrt(n)
	// hence values are bounded by 10**8
	// futhermore, we are ok for computing M(n) for n up to 10**27
	char* prime = array_alloc(primebound,  sizeof(char),
		"Could not allocate prime.");
	for (int i=0;i<primebound;i++) {
		prime[i] = true;
	}
	// sieve of eratosthenes
	prime[0]=prime[1]=false;
	int sieve_until = sqrt(primebound);
	if (s==0 && primes_verbose) printf("sieve_until:%d\n", sieve_until);
	// <= is really neccesary
	for (int i=2;i<=sieve_until;i++) {
		if (prime[i]) {
			for (int j=i*i;j<primebound;j+=i) {
				prime[j] = false;
			}
		}
	}
	
	// print_largest_primes(prime,primebound);

	// populate primes and logprimes arrays
	int primecount=0;
	for (int i=2;i<primebound;i++) {
		if (prime[i]) {
			primes[primecount] = i;
			logprimes[primecount] = ((mu_type) (floor(log2(i)))) |(mu_type)1;
			primecount++;
		}
	}
	if (primecount <= 0) {
		// overflow occurred, very unlikely
		// ( expected to occur when bound >= 10^27
		// if primecount is 32 bits )
		// but better safe than sorry
		printf("primecount overflowed \n");
		bsp_abort("");
		exit(1);
	}
	free(prime);
	if (s==0 && primes_verbose) printf("primecount: %d\n",primecount);
	return primecount;
}

/* computes the multiplication factor used in S2*/
int64_t l(int64_t y, int64_t k) {
	int64_t lowerbound = y/(k+1);
	int64_t sqrty = floor(sqrt(y));
	lowerbound = sqrty > lowerbound ? sqrty : lowerbound;
	return y/k - lowerbound;
} // end l


// for debugging
/* prints the largest primes in the array prime
   the array prime is a boolean array
   this function counts down for primebound
   printed the first 10 primes it encounters */
void print_largest_primes(char* prime,int32_t primebound) {
	int i = primebound-1;
	printf("largest primes: ");
	for (int count=0;count< 10&&i>=0;count++) {
		while (!prime[i]) { i--; }
		printf("%d ",i);
		i--; 
	}
	printf("\n");
} // end print_largest_primes

/* Computes the mobius mu function mu(x) for a <= x < b
   using the Kuznetsov fast Mobius sieve.
   Primes and logprimes must be as compute_primes_and_logprimes
   computes them.
   Primes and logprimes must contain primes and "logs" of primes
   up to at least sqrt(b) .
   Primecount specifies the length of primes and logprimes */
void compute_mu(int64_t a, int64_t b,
		 primes_type* primes, mu_type* logprimes, int primecount,
		 mu_type* mu,int progress) {
	// set most significant bit of mu[i] to 1
	// for all i.

	for (int i=0;i<b-a;i++) { write_mu(mu,i,0x80,b-a,73); }
	mu[0] = (a==0) ? 0 : mu[0];
	
	// "sieve of Möbius"
	int my_count = 0;
	int percent = 0;
	for (int i=0;i<primecount;i++) {
		if (progress && i*100/primecount > percent) {
			percent = i*100/primecount;
			printf("compute_mu: %d / 100\n",percent);
		}
		int64_t p = primes[i];
		if (p < b) { // needed in case
			// we compute mu(n) for small 
			// values of n
			int64_t rem = a%((int64_t)p);
			int64_t first_index = rem ? p - rem : 0;
			for (int j=first_index;j<b-a;j+=p) {
				mu[j] += logprimes[i];
			}
			int64_t p2 = ((int64_t)p)*p;
			if ( p2 < b) {
				//  first_index = p2 % a
				// would not work if p2 < a
				rem = a%p2;
				first_index = rem ? p2 - rem : 0;
				for (int64_t j=first_index;j<b-a;j+=p2) {
					mu[j] = 0;
				}
			}
		}
	}
	if (false) {
		for (int i=1;i<30;i++) {
			printf("%d %d %d\n",i,(int)(mu[i]&0x80),(int)mu[i]&127);
		}
	}
	// "sieve of Möbius": cleanup pass
	for (int i=0;i<b-a;i++) {
		if ((mu[i] & 0x80) == 0) {
			mu[i] = 0;
		} else {
			// we must ignore the msb 
			// therefore we do bitwise and
			if ( (int)(mu[i]&127) >= (int)(floor(log2(a+i)) - 5) ) {
			// for example
		 	// lsb == 0 means that an even number of prime factors were counted
		 	// thus if lsb == 0 tand mu[i]&127 >= (log(i)/log(2)).floor - 5, then 
	     	// mu[i] == 1
				mu[i] = 1-2*(mu[i]&1);
			} else {
				mu[i] = 2*(mu[i]&1)-1;
			}
		}
	}
	if (false) {
		for (int i=1;i<30;i++) {
			printf("c_mu_result:%d %d\n",i,(int)mu[i]);
		}
	}
} // end compute_mu

/* not used, could be used the speed up compute_mu */
void presieve(mu_type* mu, mu_type* logprimes) {
	int primes[] = {2,3,5,7,11};
	if (L!=2*2*3*3*5*7*11) {
		bsp_abort("L incompatible with presieve.");
	}
	for (int i=0;i<L;i++) {
		mu[i] = 0x80;
	}
	for (int i=0;i<5;i++) {
		int p = primes[i];
		for (int j=p;j<L;j+=p) {
			mu[j] += logprimes[i];
		}
	}
	for (int i=0;i<2;i++) {
		int p2 = primes[i] * primes[i];
		for (int j=p2;j<L;j+=p2) {
			mu[j] = 0;
		}	
	}
}

/* computes a block of 
   values of the mertens function M(x) for a <= x < b
   using the Kuznetsov fast Mobius sieve.
   Primes and logprimes must be as compute_primes_and_logprimes
   computes them.
   Primes and logprimes must contain primes and "logs" of primes
   up to at least sqrt(b) .
   Primecount specifies the length of primes and logprimes */
void compute_mertens_block(int64_t a, int64_t b,
		 primes_type* primes, mu_type* logprimes, int primecount,
		 mu_type* mu, int64_t* mertens,int64_t previous_mertens) {
	compute_mu(a,b,primes,logprimes,primecount,mu,false);
	
	// accumulate mu to compute mertens
	int zeroes = false && a == 0;
	if (zeroes) printf("mertens zeroes\n");

	int zeroindex = 0;
	int64_t sum = previous_mertens;
	for (int i=0;i<b-a;i++) {
		sum += mu[i];
		mertens[i] = sum;
		if (sum == 0 && zeroindex < 33 && zeroes) {
			printf("%d ",i);
			zeroindex++;
		}
	}
	if (zeroes) printf("\n");
	if (false && a <= 10000 && 10000 < b) {
		printf("M(10000) (should be  -23): %" PRId64 "\n",mertens[10000-a]);
	}
	if (false && a <= 1000*1000 && 1000*1000 < b) {
		printf("M(10^6) (should be  212): %" PRId64 "\n",mertens[1000000-a]);	
	}	

} // end compute_mertens_block

