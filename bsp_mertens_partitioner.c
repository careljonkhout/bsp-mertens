
/*
 * see bsp_mertens_partitioner.h for description of 
 */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <inttypes.h>

#include "aux.h"

#define false 0
#define true 1
#define bound_type uint64_t
#define bisect_tolerance 0.001


/*
 * computes C(S_1,x,u,a,b) by summation
 * this function is used to test the function s1_cost_fast
 */

double s1_cost(int64_t x, int64_t u, int64_t a, int64_t b) {
	if (b < sqrt(x/u)) {
		return 0;
	} else {
		int64_t result = 0;
		for (int64_t m = max(1,x/b/b) ; m < u ; m++) {
			int64_t ops = - max(u/m,x/b/m)-1
			              + min(sqrt(x/m),x/a/m)+1;
			result += max(0,ops);
		}
		return result;
	}
}


/*
 * computes C(S_2,x,u,a,b) by summation
 * this function is used to test the function s2_cost_fast
 */

double s2_cost(int64_t x, int64_t u, int64_t a, int64_t b) {
	int64_t result=0;
	for (int64_t k=a;k<b;k++) {
		result += min(x/k/k,u);
	}
	return result;
}
double s1_cost_fast(bound_type xx, int64_t uu, int64_t aa, int64_t bb) {
	double x = xx;
	double u = uu;
	double a = aa;
	double b = bb;
//	printf("s1_cost_fast input: %.2e %.2e %.2e %.2e\n",x,u,a,b );
	if (! (a < b)) {
		printf("invalid range s1_cost_fast\n");
		exit(1);
	}
	if (u <= x/(b*b)) {
		return 0;
	} else if (x /(b*b) <= u && u < x /(a*a)) {
		return 2 * sqrt(x) * ( sqrt(u) - sqrt(x/b/b))
			- x/b * log(u*b*b/x);
	} else if (x/a/a <= u) {
		return 2*x*(1/b-1/a) - x/b*log(a*a/b/b) + x*(1/a-1/b)*log(u*a*a/x);
	} else {
		printf("invalid argument s1_cost_fast");
		exit(1);
	}
}

double s2_cost_fast(bound_type xx, int64_t uu, int64_t aa, int64_t bb) {
	double x = xx;
	double u = uu;
	double a = aa;
	double b = bb;
//	printf("s2_cost_fast input: %.2e %.2e %.2e %.2e\n",x,u,a,b );
	if (! (a < b)) {
		printf("invalid range s2_cost_fast");
		exit(1);
	}
	if (b <= sqrt(x/u)) {
		return (b-a)*u;
	} else if (a <= sqrt(x/u) && b <= sqrt(x)) {
		return 2*sqrt(u*x) - a* u - x/b;
	} else if (a <= sqrt(x/u) /*&& sqrt(x) < b*/) {
		return 2*sqrt(u*x) - a*u - sqrt(x);	
	} else if (sqrt(x/u) < a  && b <= sqrt(x)) {
		return x*(1/a-1/b);
	} else if (a <= sqrt(x) /*&& sqrt(x) < b */) {
		return x*(1/a-1/sqrt(x));
	} else if (a > sqrt(x)) {
		return 0;
	} else {
		printf("error in s2_cost_fast");
		exit(1);		
	}
}




int S2_multiplier = 2;
int small_mertens_cost = 3;

double total_cost(bound_type x, int64_t u, int64_t a, int64_t b) {
	double S1 = s1_cost_fast(x,u,a,b);
	double S2 = S2_multiplier*s2_cost_fast(x,u,a,b);
	double sm = small_mertens_cost*(b-a);
	return S1+S2+sm;
}



int64_t bisect(bound_type x, int64_t u, int64_t lower, int64_t upper, double a) {
	// printf("a %.2e\n", a);
	int64_t mid = (lower+upper)/2;
	int i = 0;
	while (fabs(total_cost(x,u,1,mid)-a)/a > bisect_tolerance && i++ < 20) {
		if (total_cost(x,u,1,mid) < a) {
			lower = mid;
			//printf("lower = %ld total_cost %.2e\n", lower,total_cost(x,u,1,mid));
		} else {
			upper = mid;
			//printf("upper = %ld total_cost %.2e\n", upper,total_cost(x,u,1,mid));
		}
		mid = (lower+upper)/2;
	}
	//printf("%d\n",i);
	mid = (lower+upper)/2;
	//printf("mid = %ld total_cost %.2e stopping value %.2e\n", mid,total_cost(x,u,1,mid),fabs(total_cost(x,u,1,mid)-a)/a);
	return mid;
}


void find_partition(bound_type x, int64_t u, int n_cores, int64_t* partition) {
	double work = total_cost(x,u,1,x/u);
	double work_per_core = work / n_cores;
//	printf("work %.2e\n", work);
//	printf("work_per_core %.2e\n", work_per_core);
	int64_t lower = 1;
	int64_t upper = x/u;
	
	for (int i=0;i<n_cores-1;i++) {
		partition[i] = bisect(x,u,lower,upper,work_per_core*(i+1));
		lower = partition[i];
	}
}

// prints a partition computed by find_partition
void print_partition(bound_type x, int64_t u, int n_cores, int64_t* partition) {
	printf("%d %d" " %" PRId64 "\n" ,0,1,partition[0]);
	for (int i=1;i<n_cores-1;i++) {
		printf("%d %" PRId64 " %" PRId64 "\n" ,i,partition[i-1],partition[i]);
	}
	printf("%d %" PRId64 " %" PRId64 "\n" ,n_cores-1,partition[n_cores-2], x/u);
}

/**
int main(int argc, char** argv) {
	int64_t x = power_of_ten(1,15);
	int64_t u = round(pow(x,1/3.0));
	int n_cores = 4;
	int64_t partition[n_cores-1];
	find_partition(x,u,n_cores,partition);
	print_partition(n_cores,partition);
}
*/

// used to test s1_cost_fast and s2_cost_fast
void cost_test() {
	int n_blocks = 1000;
	bound_type x = power_of_ten(1,12);
	int64_t u = round(pow(x,1/3.0));
	int64_t L = (int64_t) x/u/n_blocks;
	printf("%" PRId64 " %" PRId64 " %" PRId64 "\n",x,u,L );
	double total_work = 0;
	double total_work_f= 0;
	for (int b=0;b<n_blocks;b++) {

		double S1 = s1_cost(x,u,a(b,L),a(b+1,L));
		double S2 = s2_cost(x,u,a(b,L),a(b+1,L));
		double S1f = s1_cost_fast(x,u,a(b,L),a(b+1,L));
		double S2f = s2_cost_fast(x,u,a(b,L),a(b+1,L));
		total_work += S1+S2_multiplier*S2;
		total_work_f += S1f+S2_multiplier*S2f;
		printf("%d %.2e %.2e %.2e %.2e %.2e\n",b,S1,S1f,S2,S2f,S1+2*S2);
	}
	printf("%.2e %.2e\n", total_work, total_work_f);
}
