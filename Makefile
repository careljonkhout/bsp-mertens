bsp:
	gcc bsp-mertens.c sequential_mertens.c\
	 bsp_mertens_partitioner.c aux.c libmcbsp1.2.0.a\
	 -lm -lrt -lpthread -O3\
	 --std=c99\
	 -o bsp_mertens

seq:
	gcc test_seq_mertens.c sequential_mertens.c\
	 aux.c libmcbsp1.2.0.a -lm -lrt -lpthread -O3\
	 --std=c99\
	 -o seq_mertens
	 
vec:
	gcc vec_mertens.c\
	 aux.c libmcbsp1.2.0.a -lm -lrt -lpthread -O3\
	 --std=c99\
	 -o vec_mertens

kuz:
	gcc kuz_mertens.c sequential_mertens.c\
	 aux.c libmcbsp1.2.0.a -lm -lrt -lpthread -O3\
	 --std=c99\
	 -o kuz_mertens

kuzd:
	gcc kuz_mertens.c sequential_mertens.c\
	 aux.c libmcbsp1.2.0.a -lm -lrt -lpthread -O3\
	 -g --std=c99\
	 -o kuz_mertens

mandel: mandel.c
	gcc mandel.c libmcbsp1.2.0.a -lm -lrt -lpthread -O3\
	 -g --std=c99\
	 -o mandel