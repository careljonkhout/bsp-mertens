#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <inttypes.h>
#include "mcbsp.h"
#include "mcbsp-affinity.h"
#include "sequential_mertens.h"

int main(int argc, char* argv[]) {
	bound_type bound = power_of_ten(1,12);
	int input_ok = true;
	if (argc >= 3) {
		int base;
		int exp;
		input_ok &= (sscanf(argv[1],"%d",&base) != EOF);
		input_ok &= (sscanf(argv[2],"%d",&exp ) != EOF);
		bound = power_of_ten(base,exp);
	}
	else if (argc >= 2) { // note: argv[0] = executable file name
		input_ok &= (sscanf(argv[1],"%" SCNd64,&bound) != EOF);
	} 
	if (! input_ok) {
		printf("input error");
		return 1;
	} else if (
		bound > 1000ll*1000ll*1000ll*1000ll*1000ll*1000ll+1) {
		printf("bound too big");
		return 1;
	}

	int exp = floor(log10(bound));
	int base = floor(bound/pow(10,exp));
	printf("seq_m bound:%" PRId64 "=%d*10^%d \n",bound, base, exp);
	char buf[i128_string_length];
	result_type result = sequential_mertens(bound);
	printf("result: %s\n",sprint_result(result,buf));
    return 0;
} // end main */