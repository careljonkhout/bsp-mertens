require 'benchmark'
include Math

x = 10**9
p [:x,x]
	
#u=(x**(2/3.0)).ceil
u=((x**(2/3.0))/(log(log(x))**(2/3.0))).ceil+1
max_m = u
p [:u,u]
p [:max_m, max_m]
# eratosthenes up to sqrt max_m
primebound = sqrt(max_m).floor+1
p [:primebound, primebound]
prime= [true]*primebound
prime[0]=prime[1] = false

(2...sqrt(primebound)).each do |i|
	if prime[i]
		(i*i...primebound).step(i) do |j|
			prime[j] = false
		end
	end
end

#(2..primebound).each do |i| print i, ' ' if prime[i] end
#puts


mu = [1]*max_m

#p [:prime, prime[0..30]]

(2...primebound).each do |i|
	if prime[i]
		(i*i...max_m).step(i*i) do |j|
			mu[j] = 0
		end
		(i...max_m).step(i) do |j|
			mu[j] *= -i
		end
	end
end

#p [:mu_before_normalisation, mu[0..30]]

(1...max_m).each do |i|
	if mu[i].abs < i
		mu[i] = -mu[i] 
	end
	if mu[i] > 0
		mu[i] = 1
	end
	if mu[i] < 0
		mu[i] = -1
	end
end

sum = 0


small_mertens = [nil]*max_m


(1...max_m).each do |i|
	sum += mu[i]
	small_mertens[i] = sum
end

#p [:mu, mu[0..30]]
#p([:small_mertens, small_mertens[1..30]])

print :small_mertens_equals_zero, " "
(1...max_m).each do |i|
	if small_mertens[i] == 0
		print i, ' '
	end
	break if i == 200
end
puts

@m_n_over_x = []
@small_mertens = small_mertens
@n = x
#@t=t=(@n**(2/3.0)).ceil
@u=u#=(@n**(2/3.0)).ceil
#p [:t,t]
p [:u,u] 
@minx = x
def k1(n,t)
#	p [:k1, n, t]
	sum = 0
	step = @n/n

	(2*step..@n/t).step(step) do |x|
#		p [116,:x,x,:m_n_over_x, @m_n_over_x[x]]
		if x <= @n/@u
			if @m_n_over_x[x]
				sum += @m_n_over_x[x]
			else
				puts "nil: x: #{x}"
			end
		else
			sum+=@small_mertens[@n/x]
			@minx = [@minx,@n/x].min
		end
	end
	sum
end

@counter = 0

def k2(n,t)
	sum = 0
	if @counter < 1
		verbose = true
	end
	@counter += 1
	for x in 1...t
		sum += (n/x - n/(x+1)) * @small_mertens[x]
		#p [:k2,:x,x,:n,n,:sum,sum,:nx,(n/x - n/(x+1)),:mert,@small_mertens[x]]
	end
	sum
end

(2..@n/u).reverse_each do |x|
	tt = sqrt(@n/x).ceil()
	@m_n_over_x[x] = 1-k1(@n/x,tt) -k2(@n/x,tt)
	if x<40
		p [:x,x,:k1,k1(@n/x,tt),:k2,k2(@n/x,tt),@m_n_over_x[x]]
	end
end

k1val = k1(x,@n/u)
k2val = k2(x,@n/u)

p [:kuz_with_array_k1,k1val]
p [:kuz_with_array_k2,k2val]
p [:minx,@minx]
p [:kuz_with_array,1-k1val - k2val]


if false
	# naive kuznetzov
	uu = sqrt(x).floor
	k1 = 0
	for n in 2..x/uu
		k1 += small_mertens[x/n]
	end
	p [:kuz_k1,k1]
	k2=0
	for n in 1..(uu-1)
		k2 += ll(x,uu,n) * small_mertens[n]
	end
	p [:kuz,1-k1-k2]
end

# [:x, 100000000]
# [:u, 10000.0]
# [:max_m, 10000.0]
# [:primebound, 101]
# small_mertens_equals_zero 2 39 40 58 65 93 101 145 149 150 159 160 163 164 166 
# [:kuz_with_array_k1, -23680]
# [:kuz_with_array_k2, 21753]
# [:kuz_with_array, 1928]
# [Finished in 37.0s]