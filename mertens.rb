require 'benchmark'
include Math

x = 10**6

u=(x**(1/3.0)*(log log x)).floor
p ["u smaller than max_m=x/u",u < x/u]
max_m = x/u+1
p [:u,u]
p [:max_m, max_m]

# for debugging eratosthenes up to max_m
if false
	moreprime= [true]*max_m
	moreprime[0]=moreprime[1] = false

	(2...sqrt(max_m)).each do |i|
		if moreprime[i]
			(i*i...max_m).step(i) do |j|
				moreprime[j] = false
			end
		end
	end


	if false
		moreprimes = moreprime.map.with_index do |e,i| e ? i : false end
		moreprimes.select! do |e| e end
		logprimes = moreprimes.map do |p| [p,(log(p)/log(2)).floor|1] end	
		p moreprimes
		p logprimes
	end
end

# eratosthenes up to sqrt max_m
# The byte-sized Mobius sieve 
# has one particular caveat that I will discuss
# the -5 in the line if mu[i]&127 >= ( (log(i)/log(2)).floor() -5)
# will cause the algoritm to be incorrect for small bounds (up to around 10000)
# therefore we put:
primebound = [sqrt(max_m).floor+1,[10**5,max_m].min].max
p [:primebound, primebound]
prime= [true]*primebound
prime[0]=prime[1] = false

(2...sqrt(primebound)).each do |i|
	if prime[i]
		(i*i...primebound).step(i) do |j|
			prime[j] = false
		end
	end
end

logprimes = [0]*sqrt(primebound)
# I don't think this array must be created
logprimeindex = 0
for i in 2...primebound do
	if prime[i]
		logprimes[logprimeindex] = (log(i)/log(2)).floor|1
		logprimeindex = logprimeindex + 1
	end
end


mu = [128]*max_m

for i in 2...primebound
	if prime[i]
		logprime = (log(i)/log(2)).floor|1
		(i...max_m).step(i) do |j|
			mu[j] += logprime
		end
		(i*i...max_m).step(i*i) do |j|
			mu[j] = 0
		end
	end
end


#p (mu[0..30].map.with_index do |e,i| [i,e] end)

for i in 1...max_m
	if mu[i]&128 == 0
		mu[i] = 0
	else 
		if mu[i]&127 >= ( (log(i)/log(2)).floor() -5)  # lsb == 0 --> even number of prime factors were counted
			# thus if lsb == 0 tand mu[i]&127 >= (log(i)/log(2)).floor - 5, then 
			# mu[i] == 1
			mu[i] = 1 - 2 * (mu[i] & 1) 
		else
			mu[i] = 2 * (mu[i] & 1) - 1
		end
	end
end


#p (mu[0..30].map.with_index do |e,i| [i,e] end)
#mu.each_with_index do |e,i| p [i,e] end


sum = 0
small_mertens = []
print "mertens zeroes: "
zeroindex = 0
(1...max_m).each do |i|
	sum += mu[i]
	small_mertens[i] = sum
	if sum == 0 && zeroindex < 30
		print i, " "
		zeroindex += 1
	end
end
puts

if false
	m = small_mertens[0..400]
	m.shift
	m.each_slice(20) do |slice| p slice end
end

# verification using algorithm without logs
if false
	mu_without_logs = [1]*max_m

	#p [:prime, prime[0..30]]

	(2...primebound).each do |i|
		if prime[i]
			(i*i...max_m).step(i*i) do |j|
				mu_without_logs[j] = 0
			end
			(i...max_m).step(i) do |j|
				mu_without_logs[j] *= -i
			end
		end
	end

	#p [:mu_before_normalisation, mu[0..30]]

	(1...max_m).each do |i|
		if mu_without_logs[i].abs < i
			mu_without_logs[i] = -mu_without_logs[i] 
		end
		if mu_without_logs[i] > 0
			mu_without_logs[i] = 1
		end
		if mu_without_logs[i] < 0
			mu_without_logs[i] = -1
		end
	end

	def factorize(n)
		factors = []
		j=2
		while n > 1
			while n%j == 0
				n/=j
				factors << j
			end
			j += 1
		end
		factors
	end

	error_index = 0

	(1...max_m).each do |i|
		if mu[i] != mu_without_logs[i] 
			p [:wrong, i,:factors_of_i,factorize(i),:mui, mu[i], :mu_wo_logs,mu_without_logs[i], ]
			p(factorize(i).map do |p| (log(p)/log(2)).floor|1 end.inject(:+))
			p((log(i)/log(2)).floor)
			error_index += 1
		end
		break if error_index  >= 30
	end

	puts :a
end

def ldmc y,k
	y/k - [y/(k+1),(sqrt(y)).floor].max
end


s1_blocks = 0

L = u#sqrt(u).floor*8

def a k
	# add one, or else we get division by zero
	k * L + 1
end

ncores = 4
partition = [0]
(1..ncores-1).each do |i|
	 partition << x/u/ncores * i
end
partition << x/u

list = []

s1_blocks_parts = 0
puts :s1_blocks_partitions_benchmark
puts(Benchmark.measure do
	partition.each_cons(2) do |a0,a_max|
		k_max = (a_max - a0)/L + 1 #(Float(a_max-a0)/L).ceil
		(0...k_max).each do |k|
			low = a0 + a(k)
			high = [a0+a(k+1),a_max+1].min
			(1..u).each do |m|
				if mu[m] != 0
					sum = 0
					([u/m+1,x/m/high+1].max..[sqrt(x/m),x/m/low].min).each do |n|
						#p [m,n,small_mertens[x/(n*m)]]
#						list << [:m,m,:n,n,:k,k,:lower_bound,[u/m+1,x/m/high+1].max,:upper_bound,[sqrt(x/m),x/m/low].min]
						sum += small_mertens[x/(n*m)]
					end
					s1_blocks_parts += mu[m] * sum
				end
			end
		end
	end
end)

list.sort! do |a,b| a[0..3] <=> b[0..3] end
list.each do |e| p e end

list2 = []

puts :s1_blocks_benchmark
puts(Benchmark.measure do
	(0..x/u/L).each do |k|
		(1..u).each do |m|
			if mu[m] != 0
				sum = 0
				#p [ [(u.to_f/m+1).floor,(x.to_f/m/a(k+1)+1).floor].max,[sqrt(x/m),x/m/a(k)].min]
				([u/m+1,x/m/a(k+1)+1].max..[sqrt(x/m),x/m/a(k)].min).each do |n|
					#p [m,n,small_mertens[x/(n*m)]]
					sum += small_mertens[x/(n*m)]
#					list2 << [:m,m,:n,n,:k,k,:lower_bound,(x.to_f/m/a(k+1)+1).floor,:upper_bound,x/m/a(k)]

				end
				s1_blocks += mu[m] * sum
			end
		end
	end
end)

list2.sort! do |a,b| a[0..3] <=> b[0..3] end
list2.each do |e| p e end

s1 = 0

puts :s1_plain_benchmark

puts(Benchmark.measure do 
	(1..u).each do |m|
		sum = 0
		(u/m+1..sqrt(x/m)).each do |n|
			#p [m,n,small_mertens[x/(n*m)]]
			sum += small_mertens[x/(n*m)]
		end
		s1 += mu[m] * sum
	end
end)


s2_no_blocks =0

if false
	puts (Benchmark.measure do 

		(1..u).each do |m|
			sum = 0
			(1..sqrt(x/m)).each do |k|
				sum += small_mertens[k] * ldmc(x/m,k)
			end
			s2_no_blocks += mu[m] * sum
		end

	end)
end

if false

	s2_basic = 0

	(1..u).each do |m|
		sum = 0
		if (mu[m]!=0)
			((sqrt(x/m.to_f)+1).floor..x/m).each do |n|
				sum += small_mertens[x/(n*m)]
				# http://math.stackexchange.com/questions/147771/rewriting-repeated-integer-division-with-multiplication
				# note x/n/m is the same as x(n*m)
			end
			s2_basic += mu[m]*sum
		end
	end

end

s2_blocks = 0
puts :s2_benchmark
if true
	puts (Benchmark.measure do
		
		# s2 blocks
		(1..sqrt(x)).each do |k|
			sum = 0
			(1..[u,x/(k*k)].min).each do |m|
				sum += mu[m] * ldmc(x/m,k)
			end
			s2_blocks += small_mertens[k] * sum
		end
	end)
end

p [:mu,small_mertens[u]]
p [:s1_blocks,s1_blocks]
p [:s1_blocks_parts,s1_blocks_parts]
p [:s1,s1]
p [:s2,s2_blocks]

p [:fast_result_s2_blocks,small_mertens[u]-s1-s2_blocks]

#[0,  1, 2, 3,   4,   5,   6,    7,    8,    9,     10,     11,    12,     13,      14,       15,       16]
#[1, –1, 1, 2, –23, –48, 212, 1037, 1928, –222, –33722, –87856, 62366, 599582, –875575, –3216373, –3195437]