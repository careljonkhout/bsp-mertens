#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <inttypes.h>
#include <string.h>
#include "mcbsp.h"
#include "mcbsp-affinity.h"
#include "aux.h"


int64_t k1(bound_type bound, int64_t u, int64_t n, int64_t t, int64_t* mertens, int64_t* m_n_over_x) {
	int64_t sum = 0;
	int64_t step = bound / n;
	for (int64_t x = 2*step;x<=bound/t;x+=step) {
		if (x <= bound/u) {
			sum += m_n_over_x[x];
		} else {
			sum += mertens[bound/x];
		}
	}
	return sum;
}

int64_t k2(int64_t n, int64_t t, int64_t* mertens) {
	int64_t sum = 0;
	for (int64_t x=1;x<t;x++) {
		sum += (n/x - n/(x+1)) * mertens[x];
	}
	return sum;
}


result_type kuz_mertens(bound_type bound) {
	int64_t u = floor(pow(bound,2/3.0)/
		pow(log(log(bound)),2/3.0));
	if (seq_m_verbose) printf("seq_m:u:%" PRId64 " ", u);

	
	int primebound = ceil(sqrt(u))+1;
	primebound = primebound > 10000 ? primebound : 10000;
	if (verbose) printf("primebound:%d ", primebound);

	// 2 n / log(n) is a rough upper bound estimate of 
	// number of number of primes below n
	int primesbound = ceil(2*primebound/log(primebound)); 
	if (verbose) printf("primesbound:%d ", primesbound);
	int64_t L = u;    
	if (verbose) printf("L:%" PRId64 " ", L);	


	primes_type* primes = array_alloc(primesbound, sizeof(primes_type),
		"Could not allocate primes.");
	mu_type* logprimes = array_alloc(primesbound, sizeof(mu_type),
		"Could not allocate logprimes.");
	int primecount = compute_primes_and_logprimes(
		primebound,primes,logprimes,0);

	// compute mu and mertens
	int64_t* mertens = array_alloc(L+1,sizeof(int64_t),
		"Could not allocate mertens");
	mu_type* mu = array_alloc(L+1,sizeof(mu_type),
		"Could not allocate mu_for_mertens");

	int64_t* m_n_over_x = array_alloc(u,sizeof(int64_t),
		"Could not allocate m_n_over_x");
//	int64_t last_mertens = 0;
//	int64_t tt = floor(sqrt(bound))+1;
//	int64_t mertens_u;

//	int64_t k1(bound_type bound, int64_t u, int64_t n, 
	//int64_t t, int64_t* mertens, int64_t* m_n_over_x) {
// int64_t k2(int64_t n, int64_t t, int64_t* mertens) {

	compute_mertens_block(1,L,primes,logprimes,primecount,
		mu,mertens,0);
	mertens--;
	for (int64_t x=bound/u;x>=2;x--) {
		int64_t t = ceil(sqrt(bound/x));
		int64_t k1val = k1(bound,u,bound/x,t,mertens,m_n_over_x);
		int64_t k2val = k2(bound/x,t,mertens);
		m_n_over_x[x] = 1-k1val-k2val;
	}
	int64_t k1val = k1(bound,u,bound,bound/u,mertens,m_n_over_x);
	int64_t k2val = k2(bound,bound/u,mertens);
	free(primes);
	free(logprimes);
	free(mu);
	free(++mertens);
	free(m_n_over_x);
	if (progress_indicator) printf("\n");
	return 1-k1val-k2val;
} // end sequential_mertens


int main(int argc, char* argv[]) {
	bound_type bound = power_of_ten(1,12);
	int input_ok = true;
	if (argc >= 3) {
		int base;
		int exp;
		input_ok &= (sscanf(argv[1],"%d",&base) != EOF);
		input_ok &= (sscanf(argv[2],"%d",&exp ) != EOF);
		bound = power_of_ten(base,exp);
	}
	else if (argc >= 2) { // note: argv[0] = executable file name
		input_ok &= (sscanf(argv[1],"%" SCNd64,&bound) != EOF);
	} 
	if (! input_ok) {
		printf("input error");
		return 1;
	} else if (
		bound > 1000ll*1000ll*1000ll*1000ll*1000ll*1000ll+1) {
		printf("bound too big");
		return 1;
	}

	int exp = floor(log10(bound));
	int base = floor(bound/pow(10,exp));
	printf("bound:%" PRId64 "=%d*10^%d \n",bound, base, exp);
	char buf[i128_string_length];
	result_type result = kuz_mertens(bound);
	printf("result: %s\n",sprint_result(result,buf));
    return 0;
} // end main */

