#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <inttypes.h>
#include <string.h>
#include "mcbsp.h"
#include "mcbsp-affinity.h"
#include "aux.h"
#include "sequential_mertens.h"

#define k2debug false
#undef result_type
#define result_type int64_t


int64_t k1_big(bound_type bound, int64_t u, int64_t n,
		int64_t t, result_type* m_n_over_x) {

	int64_t sum = 0;
	int64_t step = bound / n;
	int64_t xmax = min(bound/t,bound/u);
	for (int64_t x = 2*step;x<=xmax;x+=step) {
		sum += m_n_over_x[x];
	}
	return sum;
}


int64_t k1_small(bound_type bound, int64_t u, int64_t n,
		int64_t t, int64_t* mertens, int64_t k) {
	int64_t sum = 0;
	int64_t step = bound / n;
	int64_t x0 = max(max(2*step,(bound/u/step+1)*step),
		/* (bound/u/step+1)*step is the first multiple of step 
			larger than bound/u */
		 (bound/a(k+1,L)/step + 1)*step );
	int64_t x_max = min(bound/t,bound/a(k,L));
	
	for (int64_t x=x0;x<=x_max;x+=step) {
		//if (a(k,L) <=bound/x && bound/x < a(k+1,L)) {
			sum += mertens[bound/x-a(k,L)];
		//}
	}
	return sum;
}


int64_t k1_small_dbg(bound_type bound, int64_t u, int64_t n,
		int64_t t, int64_t* mertens) {
	int64_t sum = 0;
	int64_t step = bound / n;
	int64_t x0 = max(2*step,(bound/u/step+1)*step);
		/* eerste veelvoud van step groter 
							dan bound/u en 2*step*/ 
	int64_t x_max = bound/t;
	for (int64_t x=x0;x<=x_max;x+=step) {
		//if (a(k,L) <=bound/x && bound/x < a(k+1,L)) {
			sum += mertens[bound/x];
		//}
	}
	return sum;
}

int64_t k2(int64_t n, int64_t t, int64_t* mertens,int64_t k) {
	int64_t sum = 0;
	int64_t x0 = max(a(k,L),1);
	int64_t x_max = min(a(k+1,L),t);
	for (int64_t x=x0;x<x_max;x++) {
		sum += (n/x - n/(x+1)) * mertens[x-a(k,L)];
		//printf("k2:k:%ld n:%ld ak:%ld x:%ld sum:%ld mert:%ld\n",k,n,a(k,L),x,sum,mertens[x-a(k,L)]);
	}

	return sum;
}

int64_t k2dbg(int64_t n, int64_t t, int64_t* mertens) {
	int64_t sum = 0;
	int64_t x0 = 1;
	int64_t x_max = t;
	for (int64_t x=x0;x<x_max;x++) {
		sum += (n/x - n/(x+1)) * mertens[x];
		//printf("k2:k:%ld n:%ld ak:%ld x:%ld sum:%ld mert:%ld\n",k,n,a(k,L),x,sum,mertens[x-a(k,L)]);
	}

	return sum;
}



result_type kuz_mertens(bound_type bound) {
	int64_t u = floor(pow(bound,2/3.0)/
		pow(log(log(bound)),2/3.0));
	if (seq_m_verbose) printf("seq_m:u:%" PRId64 " ", u);
	
	int primebound = ceil(sqrt(u))+1;
	primebound = primebound > 10000 ? primebound : 10000;
	if (verbose) printf("primebound:%d ", primebound);

	// 2 n / log(n) is a rough upper bound estimate of 
	// number of number of primes below n
	int primesbound = ceil(2*primebound/log(primebound)); 
	if (verbose) printf("primesbound:%d ", primesbound);
	
	if (verbose) printf("L:%d ", L);	


	primes_type* primes = array_alloc(primesbound, sizeof(primes_type),
		"Could not allocate primes.");
	mu_type* logprimes = array_alloc(primesbound, sizeof(mu_type),
		"Could not allocate logprimes.");
	int primecount = compute_primes_and_logprimes(
		primebound,primes,logprimes,0);

	// compute mu and mertens
	int64_t* mertens = array_alloc(L+1,sizeof(int64_t),
		"Could not allocate mertens");
	mu_type* mu = array_alloc(L+1,sizeof(mu_type),
		"Could not allocate mu_for_mertens");

	int64_t* mertensdbg;
	mu_type* mudbg;
	if (k2debug) {
		mertensdbg = array_alloc(u+1,sizeof(int64_t),
			"Could not allocate mertensdbg");
		mudbg = array_alloc(u+1,sizeof(mu_type),
			"Could not allocate mudbg");
	}

	result_type* m_n_over_x = array_alloc(u+1,sizeof(int64_t),
		"Could not allocate m_n_over_x");
	// for debugging
	result_type* k2s;
	result_type* k1ss;
	if (k2debug) {
		k2s = array_alloc(u+1,sizeof(int64_t),
			"Could not allocate k2s");
		k1ss = array_alloc(u+1,sizeof(int64_t),
			"Could not allocate k1ss");
	}
	
	int64_t previous_mertens=0;

	for (int64_t i=1;i<=u;i++) {
		m_n_over_x[i] = 1;
		if(k2debug) {
			k2s[i] = 0;
			k1ss[i] = 0;
		}
	}
//	printf("u:%ld bound/u:%ld\n",u,bound/u);
	if (k2debug) compute_mertens_block(1,u,
			primes,logprimes,primecount,mudbg,mertensdbg,0);
	mertensdbg--;
	for (int64_t k=0;k <= u/L;k++) {
		compute_mertens_block(a(k,L),a(k+1,L),
			primes,logprimes,primecount,mu,mertens,previous_mertens);
		previous_mertens = mertens[L-1];

		for (int64_t x=bound/u;x>=1;x--) {
			int64_t t = ceil(sqrt(bound/x));	
			int64_t k1vals = k1_small(bound,u,bound/x,t,mertens,k);
			int64_t k2val = k2(bound/x,t,mertens,k);
			if (k2debug) {
				k1ss[x] += k1vals;
				k2s[x] += k2val;
			}
			m_n_over_x[x] += -k1vals-k2val;

		}
	}
	if (k2debug) {
		int counter0 = 0;
		for (int64_t x=bound/u;x>=1&& counter0<10;x--) {
			int64_t k2dbgv = k2dbg(bound/x,ceil(sqrt(bound/x)),mertensdbg);
			if (k2s[x] != k2dbgv) {
				printf("error in k2 %ld %ld %ld\n",x,k2s[x],k2dbgv);
			}
			//int64_t k1_small_dbg(bound_type bound, int64_t u, int64_t n,
			//		int64_t t, int64_t* mertens) {

			int64_t k1dbgv = k1_small_dbg(bound,u,bound/x,ceil(sqrt(bound/x)),mertensdbg);
			if (k1ss[x] != k1dbgv) {
				printf("error in k1 %ld %ld %ld\n",x,k1ss[x],k1dbgv);	
				counter0++;
			}
		}
	}
	int counter =0;
	for (int64_t x=bound/u;x>=1;x--) {
		int64_t t = ceil(sqrt(bound/x));
		int64_t k1valb = k1_big  (bound,u,bound/x,t,m_n_over_x);
		m_n_over_x[x] -= k1valb;
		if (x<100) {
	//		printf("M(%ld)=%ld\n",bound/x,m_n_over_x[x]);
		}
		if (false && counter < 10) {
			result_type smnox = sequential_mertens(bound/x);
			if (m_n_over_x[x] != smnox) {
				printf("error! x:%ld mnox:%ld smnox:%ld \n",x,m_n_over_x[x],smnox);
				counter++;
			}
		}
	}

	// printing results
	int exp = 0;
	bound_type bcopy = bound;
	while (bcopy % 10 == 0) {
		exp++;
		bcopy/=10;
	}
	int xexp = 0;
	for (int64_t x = 1;x<bound/u;x*=10) {
		printf("M(%ld*10^%d)=%ld\n",bcopy,exp-xexp,m_n_over_x[x]);
		xexp++;
	}


	result_type result = m_n_over_x[1];
	free(primes);
	free(logprimes);
	free(mu);
	free(mertens);
	free(m_n_over_x);
	if (progress_indicator) printf("\n");
	return result;
} // end sequential_mertens


int main(int argc, char* argv[]) {
	bound_type bound = power_of_ten(1,12);
	int input_ok = true;
	if (argc >= 3) {
		int base;
		int exp;
		input_ok &= (sscanf(argv[1],"%d",&base) != EOF);
		input_ok &= (sscanf(argv[2],"%d",&exp ) != EOF);
		bound = power_of_ten(base,exp);
	}
	else if (argc >= 2) { // note: argv[0] = executable file name
		input_ok &= (sscanf(argv[1],"%" SCNd64,&bound) != EOF);
	} 
	if (! input_ok) {
		printf("input error");
		return 1;
	} else if (
		bound > 1000ll*1000ll*1000ll*1000ll*1000ll*1000ll+1) {
		printf("bound too big");
		return 1;
	}

	int exp = floor(log10(bound));
	int base = floor(bound/pow(10,exp));
	printf("bound:%" PRId64 "=%d*10^%d \n",bound, base, exp);
	char buf[i128_string_length];
	result_type result = kuz_mertens(bound);
	printf("result: %s\n",sprint_result(result,buf));
    return 0;
} // end main */

